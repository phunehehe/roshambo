# Roshambo

[![pipeline status](https://gitlab.com/phunehehe/roshambo/badges/master/pipeline.svg)](https://gitlab.com/phunehehe/roshambo/pipelines)
[![coverage report](https://gitlab.com/phunehehe/roshambo/badges/master/coverage.svg)](https://phunehehe.gitlab.io/roshambo/excoveralls.html)

This is a server for Rock paper scissors games. Through the API users can:

- Observe games
- View the leaderboard
- Create accounts to play games

Each game has a few rounds. The user winning more rounds is the winner of the game. Users with higher game win rate appears higher in the leaderboard.

A somewhat interesting feature is round timeout. If a user does not choose a move within the time limit, a random move is chosen and the round automatically finalizes.

For academic purposes different state management strategies are used:

- Accounts are implemented with [Agent](https://hexdocs.pm/elixir/Agent.html)
- The leaderboard is implemented with [ETS](http://erlang.org/doc/man/ets.html)
- Games are implemented with [GenServer](https://hexdocs.pm/elixir/GenServer.html)

## Development

To start your Phoenix server:

- Install dependencies with `mix deps.get`
- Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

[websocat](https://github.com/vi/websocat) can be used to interact with the games.

### TODOs

- There is currently no persistence whatsoever
- Old data could probably be cleaned up
- Maybe round timers should be cancelled

## API

### Accounts

When a user register for an account, a token will be returned for authentication later:

```bash
# command
curl localhost:4000/accounts --data username=u1 --data password=u1
# output
{"token":"SFMyNTY.g3QAAAACZAAEZGF0YXQAAAABZAAIdXNlcm5hbWVtAAAAAnUxZAAGc2lnbmVkbgYAC_8OqnEB.Bhqs7I0hrTUM4Dqujxb82C8r2DXZ6oKuMurcCUQ_LC8"}
```

Alternatively, with an existing account a user can retrieve a token any time:

```bash
# command
curl localhost:4000/accounts/tokens --data username=u2 --data password=u2
# output
{"token":"SFMyNTY.g3QAAAACZAAEZGF0YXQAAAABZAAIdXNlcm5hbWVtAAAAAnUyZAAGc2lnbmVkbgYATcuMqnEB.NbKcaX7LKuIQGqLvbouT4G9mZ42KjWv2Lu9yo6rF8iw"}
```

### Games

To begin playing, a user first connects using WebSocket using the authentication token:

```bash
websocat 'ws://localhost:4000/socket/websocket?token=SFMyNTY.g3QAAAACZAAEZGF0YXQAAAABZAAIdXNlcm5hbWVtAAAAAnUxZAAGc2lnbmVkbgYAC_8OqnEB.Bhqs7I0hrTUM4Dqujxb82C8r2DXZ6oKuMurcCUQ_LC8'
```

Join the lobby to receive game notifications:

```json
// input
{"topic":"game:lobby","event":"phx_join","payload":{},"ref":{}}
// output
{"event":"phx_reply","payload":{"response":{},"status":"ok"},"ref":{},"topic":"game:lobby"}
```

Create a game:

```json
// input
{"topic":"game:lobby","event":"create_game","payload":{},"ref":{}}
// output
{"event":"phx_reply","payload":{"response":{"id":"f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","player1":"u1"},"status":"ok"},"ref":{},"topic":"game:lobby"}
// broadcast
{"event":"game_created","payload":{"id":"f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","player1":"u1"},"ref":null,"topic":"game:lobby"}
// input
{"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","event":"phx_join","payload":{},"ref":{}}
// output
{"event":"phx_reply","payload":{"response":{},"status":"ok"},"ref":{},"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"}
```

Then another user can join the game:

```json
// input
{"topic":"game:lobby","event":"join_game","payload":{"game_id":"f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"},"ref":{}}
// output
{"event":"phx_reply","payload":{"response":{"id":"f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","player1":"u1","player2":"u2","rounds":{"1":{"id":1},"2":{"id":2}}},"status":"ok"},"ref":{},"topic":"game:lobby"}
// broadcast
{"event":"game_started","payload":{"id":"f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","player1":"u1","player2":"u2","rounds":{"1":{"id":1},"2":{"id":2}}},"ref":null,"topic":"game:lobby"}
{"event":"round_started","payload":{"round_id":1},"ref":null,"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"}
```

During each round, each user can make a move:

```json
// input
{"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","event":"make_move","payload":{"round_id":1,"move":"rock"},"ref":{}}
// output
{"event":"phx_reply","payload":{"response":{},"status":"ok"},"ref":{},"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"}
```

After both users make their move the round result is sent and the next round starts:

```json
// input
{"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660","event":"make_move","payload":{"round_id":1,"move":"rock"},"ref":{}}
// output
{"event":"phx_reply","payload":{"response":{},"status":"ok"},"ref":{},"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"}
// broadcast
{"event":"round_finished","payload":{"id":1,"move1":"rock","move2":"rock","result":"draw"},"ref":null,"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"}
{"event":"round_started","payload":{"id":2},"ref":null,"topic":"game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"}
```

When all rounds finish the game result is sent:

```json
{
  "event": "game_finished",
  "payload": {
    "id": "f9d9a8a0-afe6-43e9-a6af-a4956cb3a660",
    "player1": "u1",
    "player2": "u2",
    "result": "player2",
    "rounds": {
      "1": {
        "id": 1,
        "move1": "rock",
        "move2": "rock",
        "result": "draw"
      },
      "2": {
        "id": 2,
        "move1": "rock",
        "move2": "paper",
        "result": "player2"
      }
    },
    "win_count1": 0,
    "win_count2": 1
  },
  "ref": null,
  "topic": "game:f9d9a8a0-afe6-43e9-a6af-a4956cb3a660"
}
```

### Leaderboard

There is a simple leaderboard showing users ordered by win rate:

```bash
# command
curl localhost:4000/leaderboard
# output
[["Alice",{"game_count":2,"win_count":1}],["Carol",{"game_count":2,"win_count":1}],["Bob",{"game_count":2,"win_count":0}]]%
```
