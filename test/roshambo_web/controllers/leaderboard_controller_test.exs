defmodule RoshamboWeb.LeaderboardControllerTest do
  use RoshamboWeb.ConnCase, async: false
  alias Roshambo.Leaderboard

  test "lists leader", %{conn: conn} do
    conn = get(conn, "/leaderboard")
    assert conn |> json_response(200) |> Enum.empty?()

    Leaderboard.update("user2", "user1", :player2)
    conn = get(conn, "/leaderboard")

    assert json_response(conn, 200) == [
             ["user1", %{"game_count" => 1, "win_count" => 1}],
             ["user2", %{"game_count" => 1, "win_count" => 0}]
           ]

    Leaderboard.update("user3", "user1", :player1)
    conn = get(conn, "/leaderboard")

    assert json_response(conn, 200) == [
             ["user3", %{"game_count" => 1, "win_count" => 1}],
             ["user1", %{"game_count" => 2, "win_count" => 1}],
             ["user2", %{"game_count" => 1, "win_count" => 0}]
           ]

    Leaderboard.update("user3", "user2", :draw)
    Leaderboard.update("user3", "user2", :draw)
    conn = get(conn, "/leaderboard")

    assert json_response(conn, 200) == [
             ["user1", %{"game_count" => 2, "win_count" => 1}],
             ["user3", %{"game_count" => 3, "win_count" => 1}],
             ["user2", %{"game_count" => 3, "win_count" => 0}]
           ]

    Leaderboard.clear()
    Process.sleep(10)
  end
end
