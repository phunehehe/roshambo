defmodule RoshamboWeb.AccountControllerTest do
  use RoshamboWeb.ConnCase
  alias RoshamboWeb.AccountController

  test "creates account and token", %{conn: conn} do
    conn = post(conn, "/accounts", username: "test username", password: "test password")
    token = json_response(conn, 201)["token"]
    assert AccountController.verify_token(token) == {:ok, %{username: "test username"}}

    conn = post(conn, "/accounts", username: "test username", password: "whatever")
    body = json_response(conn, 422)
    assert body == %{"error" => "username_taken"}

    conn = post(conn, "/accounts/tokens", username: "test username", password: "test password")
    token = json_response(conn, 200)["token"]
    assert AccountController.verify_token(token) == {:ok, %{username: "test username"}}
  end
end
