defmodule RoshamboWeb.GameChannelTest do
  use RoshamboWeb.ChannelCase, async: false

  alias Roshambo.{Games, Leaderboard}
  alias RoshamboWeb.{GameChannel, UserSocket}

  @timeout Application.get_env(:roshambo, :round_timeout) * 2

  setup do
    {:ok, _, socket1} =
      UserSocket
      |> socket(nil, %{username: "user 1"})
      |> subscribe_and_join(GameChannel, "game:lobby")

    {:ok, _, socket2} =
      UserSocket
      |> socket(nil, %{username: "user 2"})
      |> subscribe_and_join(GameChannel, "game:lobby")

    {:ok, %{socket1: socket1, socket2: socket2}}
  end

  test "creates, joins, and plays game", %{socket1: socket1, socket2: socket2} do
    socket1
    |> push("create_game")
    |> assert_reply(:ok, %{id: game_id, player1: "user 1"})

    {:ok, _, socket1} = subscribe_and_join(socket1, GameChannel, "game:#{game_id}")
    assert_broadcast "game_created", %{id: ^game_id, player1: "user 1"}

    socket2
    |> push("join_game", %{game_id: game_id})
    |> assert_reply(:ok, %{id: ^game_id, player1: "user 1", player2: "user 2"})

    {:ok, _, socket2} = subscribe_and_join(socket2, GameChannel, "game:#{game_id}")
    assert_broadcast "game_started", %{id: ^game_id, player1: "user 1", player2: "user 2"}
    assert_broadcast "round_started", %{id: 1}

    socket1
    |> push("make_move", %{round_id: 1, move: "rock"})
    |> assert_reply(:ok)

    push(socket2, "make_move", %{round_id: 1, move: "paper"})
    assert_broadcast "round_finished", %{id: 1, move1: "rock", move2: "paper", result: :player2}
    assert_broadcast "round_started", %{id: 2}

    push(socket1, "make_move", %{round_id: 2, move: "scissors"})
    push(socket2, "make_move", %{round_id: 2, move: "scissors"})

    assert_broadcast "round_finished", %{
      id: 2,
      move1: "scissors",
      move2: "scissors",
      result: :draw
    }

    assert_broadcast "game_finished", %{
      id: ^game_id,
      win_count1: 0,
      win_count2: 1,
      result: :player2
    }

    Process.sleep(10)

    assert Leaderboard.list() == [
             ["user 2", %{game_count: 1, win_count: 1}],
             ["user 1", %{game_count: 1, win_count: 0}]
           ]

    Leaderboard.clear()
    Process.sleep(10)
  end

  test "picks random moves on timeout", %{socket1: socket1, socket2: socket2} do
    socket1
    |> push("create_game")
    |> assert_reply(:ok, %{id: game_id, player1: "user 1"})

    push(socket2, "join_game", %{game_id: game_id})

    assert_broadcast "round_finished", %{id: 1}, @timeout
    assert_broadcast "round_finished", %{id: 2}, @timeout
    assert_broadcast "game_finished", %{id: ^game_id}

    # This tests the randomness. There's a 1/27 chance it will fail.
    assert game_id
           |> Games.get_game()
           |> Map.get(:rounds)
           |> Map.values()
           |> Enum.flat_map(&[&1.move1, &1.move2])
           |> Enum.uniq()
           |> Enum.count() > 1

    Leaderboard.clear()
    Process.sleep(10)
  end
end
