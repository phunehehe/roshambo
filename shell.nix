let
  pkgs = import <nixpkgs> {};
  inherit (pkgs) lib stdenv;

in stdenv.mkDerivation rec {
  name = "roshambo";

  buildInputs = [
    pkgs.elixir
  ];

  installPhase = ''
    mkdir $out
    ${lib.concatMapStringsSep "\n" (i: "ln -s ${i} $out/") buildInputs}
  '';

  phases = ["installPhase"];
}
