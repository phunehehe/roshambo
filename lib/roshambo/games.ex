defmodule Roshambo.Games do
  @moduledoc """
  Store and manage games
  """

  use GenServer

  @round_count Application.get_env(:roshambo, :round_count)
  @moves ~w(rock paper scissors)

  @impl true
  def init(games) do
    {:ok, games}
  end

  @impl true
  def handle_call({:create_game, username}, _from, games) do
    id = UUID.uuid4()
    game = %{id: id, player1: username}
    {:reply, game, Map.put(games, id, game)}
  end

  def handle_call({:join_game, game_id, username}, _from, games) do
    with {:ok, game} <- fetch_game(games, game_id),
         false <- Map.has_key?(game, :player2) do
      rounds = Map.new(1..@round_count, &{&1, %{id: &1}})
      game = Map.merge(game, %{player2: username, rounds: rounds})
      {:reply, {:ok, game}, Map.put(games, game_id, game)}
    else
      true -> {:reply, {:error, :game_taken}, games}
      error -> {:reply, error, games}
    end
  end

  def handle_call({:game_exists?, game_id}, _from, games) do
    {:reply, Map.has_key?(games, game_id), games}
  end

  def handle_call({:make_move, game_id, round_id, username, move}, _from, games) do
    with :ok <- validate_move(move),
         {:ok, game} <- fetch_game(games, game_id),
         {:ok, player} <- fetch_player(game, username),
         {:ok, round} <- fetch_round(game, round_id),
         :ok <- validate_round(round) do
      round =
        round
        |> Map.put(player, move)
        |> put_round_result

      game =
        game
        |> put_in([:rounds, round_id], round)
        |> put_game_result

      games = Map.put(games, game_id, game)
      {:reply, {:ok, round}, games}
    else
      error -> {:reply, error, games}
    end
  end

  def handle_call({:force_round, game_id, round_id}, _from, games) do
    with {:ok, game} <- fetch_game(games, game_id),
         {:ok, round} <- fetch_round(game, round_id),
         :ok <- validate_round(round) do
      round =
        round
        |> Map.put_new_lazy(:move1, &random_move/0)
        |> Map.put_new_lazy(:move2, &random_move/0)
        |> put_round_result

      game =
        game
        |> put_in([:rounds, round_id], round)
        |> put_game_result

      games = Map.put(games, game_id, game)
      {:reply, {:ok, round}, games}
    else
      error -> {:reply, error, games}
    end
  end

  def handle_call({:get_game, game_id}, _from, games) do
    {:reply, games[game_id], games}
  end

  def start_link([]) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def create_game(username) do
    GenServer.call(__MODULE__, {:create_game, username})
  end

  def game_exists?(id) do
    GenServer.call(__MODULE__, {:game_exists?, id})
  end

  def join_game(game_id, username) do
    GenServer.call(__MODULE__, {:join_game, game_id, username})
  end

  def make_move(game_id, round_id, username, move) do
    GenServer.call(__MODULE__, {:make_move, game_id, round_id, username, move})
  end

  def get_game(id) do
    GenServer.call(__MODULE__, {:get_game, id})
  end

  def force_round(game_id, round_id) do
    GenServer.call(__MODULE__, {:force_round, game_id, round_id})
  end

  defp validate_round(round) do
    if Map.has_key?(round, :result) do
      {:error, :round_finished}
    else
      :ok
    end
  end

  defp fetch_game(games, game_id) do
    case Map.fetch(games, game_id) do
      :error -> {:error, :game_not_found}
      result -> result
    end
  end

  defp fetch_round(game, round_id) do
    case Map.fetch(game.rounds, round_id) do
      :error -> {:error, :round_not_found}
      result -> result
    end
  end

  defp validate_move(move) do
    if move in @moves do
      :ok
    else
      {:error, :invalid_move}
    end
  end

  defp fetch_player(game, username) do
    cond do
      username == game.player1 -> {:ok, :move1}
      username == game.player2 -> {:ok, :move2}
      true -> {:error, :not_a_player}
    end
  end

  defp put_round_result(round = %{move1: move1, move2: move2}) do
    results = %{
      "rock" => %{
        "rock" => :draw,
        "paper" => :player2,
        "scissors" => :player1
      },
      "paper" => %{
        "rock" => :player1,
        "paper" => :draw,
        "scissors" => :player2
      },
      "scissors" => %{
        "rock" => :player2,
        "paper" => :player1,
        "scissors" => :draw
      }
    }

    Map.put(round, :result, results[move1][move2])
  end

  defp put_round_result(round) do
    round
  end

  defp put_game_result(game) do
    {count1, count2, finished} =
      Enum.reduce(game.rounds, {0, 0, true}, fn {_round_id, round}, {c1, c2, f} ->
        f = f && Map.has_key?(round, :result)

        {c1, c2} =
          case Map.get(round, :result) do
            :player1 -> {c1 + 1, c2}
            :player2 -> {c1, c2 + 1}
            _ -> {c1, c2}
          end

        {c1, c2, f}
      end)

    game = Map.merge(game, %{win_count1: count1, win_count2: count2})

    if finished do
      result =
        cond do
          count1 > count2 -> :player1
          count2 > count1 -> :player2
          true -> :draw
        end

      Map.put(game, :result, result)
    else
      game
    end
  end

  defp random_move do
    Enum.random(@moves)
  end
end
