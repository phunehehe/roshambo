defmodule Roshambo.Accounts do
  @moduledoc """
  Store and manage user accounts
  """

  use Agent

  def start_link([]) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def create(username, password) do
    Agent.get_and_update(__MODULE__, fn accounts ->
      if Map.has_key?(accounts, username) do
        {{:error, :username_taken}, accounts}
      else
        {:ok, Map.put(accounts, username, password)}
      end
    end)
  end

  def validate(username, password) do
    case Agent.get(__MODULE__, &Map.fetch(&1, username)) do
      {:ok, existing_password} -> existing_password == password
      _ -> false
    end
  end
end
