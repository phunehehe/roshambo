defmodule Roshambo.Leaderboard do
  @moduledoc """
  Store and manage the leaderboard
  """
  use GenServer

  def start_link([]) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  @impl true
  def init(%{}) do
    :ets.new(__MODULE__, [:named_table, :private])
    {:ok, nil}
  end

  @impl true
  def terminate(_reason, nil) do
    :ets.delete(__MODULE__)
  end

  @impl true
  def handle_call({:update, user1, user2, result}, _from, nil) do
    :ets.update_counter(__MODULE__, user1, update_ops(result, :player1), {nil, 0, 0})
    :ets.update_counter(__MODULE__, user2, update_ops(result, :player2), {nil, 0, 0})
    {:reply, :ok, nil}
  end

  def handle_call(:list, _from, nil) do
    result =
      __MODULE__
      |> :ets.tab2list()
      |> Enum.sort_by(fn {_username, game_count, win_count} -> win_count / game_count end, &>=/2)
      |> Enum.map(fn {username, game_count, win_count} ->
        [username, %{game_count: game_count, win_count: win_count}]
      end)

    {:reply, result, nil}
  end

  def handle_call(:clear, _from, nil) do
    :ets.delete_all_objects(__MODULE__)
    {:reply, :ok, nil}
  end

  defp update_ops(result, user) do
    if result == user do
      [{2, 1}, {3, 1}]
    else
      [{2, 1}]
    end
  end

  def update(user1, user2, result) do
    GenServer.call(__MODULE__, {:update, user1, user2, result})
  end

  def list do
    GenServer.call(__MODULE__, :list)
  end

  def clear do
    GenServer.call(__MODULE__, :clear)
  end
end
