defmodule RoshamboWeb.LeaderboardController do
  use RoshamboWeb, :controller

  alias Roshambo.Leaderboard

  def list(conn, _params) do
    json(conn, Leaderboard.list())
  end
end
