defmodule RoshamboWeb.AccountController do
  use RoshamboWeb, :controller
  @salt Atom.to_string(__MODULE__)

  alias Roshambo.Accounts
  alias RoshamboWeb.Endpoint

  def create(conn, %{"username" => username, "password" => password}) do
    case Accounts.create(username, password) do
      :ok -> conn |> put_status(201) |> json(%{token: sign_token(%{username: username})})
      {:error, code} -> conn |> put_status(422) |> json(%{error: code})
    end
  end

  def create(conn, _params) do
    conn |> put_status(422) |> json(%{error: :missing_parameters})
  end

  def create_token(conn, params) do
    username = params["username"]

    if Accounts.validate(username, params["password"]) do
      json(conn, %{token: sign_token(%{username: username})})
    else
      json(conn, %{error: :invalid_credentials})
    end
  end

  defp sign_token(payload) do
    Phoenix.Token.sign(Endpoint, @salt, payload)
  end

  def verify_token(token) do
    Phoenix.Token.verify(Endpoint, @salt, token, max_age: 86_400)
  end
end
