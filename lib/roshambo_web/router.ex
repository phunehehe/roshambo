defmodule RoshamboWeb.Router do
  use RoshamboWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RoshamboWeb do
    pipe_through :api

    post "/accounts", AccountController, :create
    post "/accounts/tokens", AccountController, :create_token

    get "/leaderboard", LeaderboardController, :list
  end
end
