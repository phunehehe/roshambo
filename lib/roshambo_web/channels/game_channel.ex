defmodule RoshamboWeb.GameChannel do
  @moduledoc "
  Manage game interactions
  "

  use RoshamboWeb, :channel

  alias Roshambo.{Games, Leaderboard}
  alias RoshamboWeb.Endpoint

  @round_timeout Application.get_env(:roshambo, :round_timeout)

  @impl true
  def join("game:lobby", _payload, socket) do
    {:ok, socket}
  end

  def join("game:" <> game_id, _payload, socket) do
    if Games.game_exists?(game_id) do
      {:ok, socket}
    else
      {:error, %{code: :game_not_found}}
    end
  end

  @impl true
  def handle_in(_, _, socket = %{assigns: assigns}) when not is_map_key(assigns, :username) do
    {:reply, {:error, %{code: :account_required}}, socket}
  end

  def handle_in("create_game", _payload, socket) do
    game = Games.create_game(socket.assigns.username)
    broadcast(socket, "game_created", game)
    {:reply, {:ok, game}, socket}
  end

  def handle_in("join_game", %{"game_id" => game_id}, socket) do
    case Games.join_game(game_id, socket.assigns.username) do
      {:ok, game} ->
        broadcast(socket, "game_started", game)
        start_round(game_id, 1)
        {:reply, {:ok, game}, socket}

      {:error, code} ->
        {:reply, {:error, %{code: code}}, socket}
    end
  end

  def handle_in("make_move", %{"round_id" => round_id, "move" => move}, socket) do
    "game:" <> game_id = socket.topic

    case Games.make_move(game_id, round_id, socket.assigns.username, move) do
      {:ok, round} ->
        if Map.has_key?(round, :result) do
          finalize_round(round_id, round, game_id, socket)
        end

        {:reply, :ok, socket}

      {:error, code} ->
        {:reply, {:error, %{code: code}}, socket}
    end
  end

  @impl true
  def handle_info({:force_round, game_id, round_id}, socket) do
    case Games.force_round(game_id, round_id) do
      {:ok, round} -> finalize_round(round_id, round, game_id, socket)
      {:error, :round_finished} -> :ok
    end

    {:noreply, socket}
  end

  defp finalize_round(round_id, round, game_id, socket) do
    broadcast(socket, "round_finished", round)
    game = Games.get_game(game_id)

    if Map.has_key?(game, :result) do
      broadcast(socket, "game_finished", game)
      Leaderboard.update(game.player1, game.player2, game.result)
      # The game could be deleted now
    else
      start_round(game_id, round_id + 1)
    end
  end

  defp start_round(game_id, round_id) do
    topic = "game:#{game_id}"
    Endpoint.broadcast(topic, "round_started", %{id: round_id})

    # This timer could be stored so it can be cancelled when the users make moves
    Process.send_after(self(), {:force_round, game_id, round_id}, @round_timeout)
  end
end
