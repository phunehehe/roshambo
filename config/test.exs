use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :roshambo, RoshamboWeb.Endpoint,
  http: [port: 4002],
  server: false

config :roshambo, round_count: 2
config :roshambo, round_timeout: 100

# Print only warnings and errors during test
config :logger, level: :warn
